package tv.olaris.android.service.http

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okio.IOException
import tv.olaris.android.service.http.model.LoginRequest
import tv.olaris.android.service.http.model.LoginResponse
import java.net.SocketTimeoutException

class OlarisHttpServiceImpl(
    private val okHttpClient: OkHttpClient,
) : OlarisHttpService {

    override suspend fun getVersion(baseUrl: String): String = withContext(Dispatchers.IO) {
        val versionURL = "$baseUrl/olaris/m/v1/version"
        return@withContext try {
            okHttpClient.getVersionResponse(versionURL).toString()
        } catch (e: SocketTimeoutException) {
            Log.e("olarisHttpServer", "Received an error: ${e.message}")
            "Request timed out"
        } catch (e: IOException) {
            if (e.message == "404") {
                ""
            } else {
                Log.e("olarisHttpServer", "Received an error: ${e.message}")
                "TODO"
            }
        }
    }

    override suspend fun loginUser(
        baseUrl: String,
        username: String,
        password: String,
    ): LoginResponse = withContext(Dispatchers.IO) {
        val authLoginUrl = "$baseUrl/olaris/m/v1/auth"
        val loginRequest = LoginRequest(username, password)

        Log.d("olarisHttpServer", "Login URL: $authLoginUrl")

        try {
            val clientResponse = okHttpClient.getLoginResponse(authLoginUrl, loginRequest).toString()
            val loginResponse: LoginResponse = Json.decodeFromString(clientResponse)

            Log.d("http", "Login response $loginResponse")
            return@withContext loginResponse
        } catch (e: IOException) {
            Log.e("OlarisHTTPService", e.stackTraceToString())
            return@withContext LoginResponse(true, e.stackTraceToString())
        }
    }
}
