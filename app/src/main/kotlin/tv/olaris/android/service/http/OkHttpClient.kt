package tv.olaris.android.service.http

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.logging.HttpLoggingInterceptor
import okio.IOException
import tv.olaris.android.service.http.model.LoginRequest
import java.net.URL
import java.util.concurrent.TimeUnit

val okHttpClient = OkHttpClient.Builder()
    .connectTimeout(2, TimeUnit.SECONDS)
    .writeTimeout(3, TimeUnit.SECONDS)
    .readTimeout(10, TimeUnit.SECONDS)
    .addNetworkInterceptor(HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    })
    .addInterceptor { chain: Interceptor.Chain ->
        val original: Request = chain.request()
        val builder: Request.Builder =
            original.newBuilder().method(original.method, original.body)
        chain.proceed(builder.build())
    }
    .build()

fun OkHttpClient.getVersionResponse(url: String): String? {
    var result: String? = null
    try {
        // Create URL
        val _url = URL(url)
        // Build request
        val request = Request.Builder()
            .url(_url)
            .build()
        // Execute request
        val response = this.newCall(request).execute()
        if  (response.isSuccessful) {
            result = response.body?.string()
        } else {
            throw IOException(response.code.toString())
        }
    }
    catch(err:Error) {
        print("Error when executing get request: ${err.localizedMessage}")
    }
    return result
}

fun OkHttpClient.getLoginResponse(url: String, loginRequest: LoginRequest): String? {
    val mediaTypeMarkdown = "application/json; charset=utf-8".toMediaType()
    val requestBody = Json.encodeToString(loginRequest).toRequestBody(mediaTypeMarkdown)

    var result: String? = null
    try {
        // Create URL
        val _url = URL(url)
        // Build request
        val request = Request.Builder()
            .url(_url)
            .post(requestBody)
            .build()
        // Execute request
        val response = this.newCall(request).execute()
        if  (response.isSuccessful) {
            result = response.body?.string()
        } else {
            throw IOException(response.code.toString())
        }
    }
    catch(err:Error) {
        print("Error when executing get request: ${err.localizedMessage}")
    }
    return result
}
